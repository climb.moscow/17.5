﻿#include <iostream>
#include <math.h>

class Vector
{
    float x;
    float y;
    float z;
public:
    void SetVector(float a, float b, float c)
    {
        x = a;
        y = b;
        z = c;

    }
    void Distance()
    {
        std::cout << sqrt(x*x + y*y + z*z);
    }
};

int main()
{
    Vector point;
    point.SetVector(5, 10, 15);
    point.Distance();
}
